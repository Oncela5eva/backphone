
// Liste des députés
var mps = [];

// Groupes et circo possibles
var groups = [];
var counties = [];

// Valeur par défaut du groupes et département selectionnés
var group = 0;
var county = 0;

// Députés sélectionnées
var selected = [];
var current_mp = 0;

// L'adresse du JSON contenant les informations sur les députés
var url = 'https://www.laquadrature.net/wp-content/scripts/deputes_janvier_2020.json';

// Récupère les informations sur les députés
onload = function ()
{
	var server = new XMLHttpRequest();
	server.open("GET", url, true);
	server.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	server.send();

	server.onreadystatechange = function()
	{
		if (this.readyState == XMLHttpRequest.DONE && this.status == 200)
		{
			mps = JSON.parse(server.responseText);
			construct(mps);
		}
	}
}

// Construit le formulaire à partir des informations récupérées
function construct(mps)
{

    // Récupère les noms de groupe et de circo
	for (var i = 0; i < mps.length; i++)
    {
		if (counties.indexOf(mps[i].county) == -1)
			counties.push(mps[i].county);
		if (groups.indexOf(mps[i].group) == -1)
			groups.push(mps[i].group);
	}

	// Remplit le selecteur de groupe
	groups.sort();
	for (var i = 0; i < groups.length; i++)
	{
		var option = document.createElement("option");
			option.value = groups[i];
			option.innerHTML = groups[i];
			document.getElementById("select_group").appendChild(option);
	}

    // Gère la sélection de groupe dans le formulaire
	document.getElementById("select_group").onchange = function()
	{
		group = this.value;
		filter();
	}

	// Remplit le selecteur de circo
	counties.sort();
	for (var i = 0; i < counties.length; i++)
	{
		var option = document.createElement("option");
			option.value = counties[i];
			option.innerHTML = counties[i];
			document.getElementById("select_county").appendChild(option);
	}

    // Gère la sélection de crico dans le formulaire
	document.getElementById("select_county").onchange = function()
	{
		county = this.value;
		filter();
	}

	// Lance le premier filtre
	filter();
}

// Filtre les députés à afficher en fonction du groupe et de la circo
function filter()
{
    // Recupère la liste de députés correspondant au filtre
	selected = [];
	for (var i = 0; i < mps.length; i++)
	{
		if ( (group == 0 || mps[i].group == group)
		&& 	 (county == 0 || mps[i].county == county) )
		{
			selected.push(mps[i]);
		}
	}

	// Met à jour le nombre de députés indiqués dans le filtre
	document.getElementById("mp_total").innerHTML = selected.length;

	// Choisit un député au hasard dans le groupe
	current_mp = Math.floor(Math.random()*selected.length);

	// Masque/affiche les info sur le député si la liste est vide ou non
	if (selected.length)
	{
		document.getElementById("mp_info").style.display = "block";
		shuffle();
	}
	else
	{
        document.getElementById("mp_photo").style.backgroundImage = "url()";
        document.getElementById("mp_info").style.display = "none";
	 }
}

// Sélectionne un nouveau député
document.getElementById("mp_next").onclick = shuffle;

function shuffle()
{
	current_mp++;
	if (current_mp == selected.length)
		current_mp = 0;

	// Met à jour les informations à afficher
	update();
}


// Met à jour les informations à afficher
function update()
{
	var mp = selected[current_mp];

	document.getElementById("mp_name").innerHTML = mp.name;
	document.getElementById("mp_photo").style.backgroundImage = "url('https://www.laquadrature.net/wp-content/deputes/"+mp.id+".jpg')";
    document.getElementById("mp_group").innerHTML = mp.group;
	document.getElementById("mp_county").innerHTML = mp.county;
    document.getElementById("mp_mail").href = "mailto:" + mp.email;
	document.getElementById("mp_mail").innerHTML = mp.email;

    document.getElementById("mp_phone").href = "tel:+33" + mp.phone.substring(1).replace(/ /g, "");
    document.getElementById("mp_phone").innerHTML = mp.phone;

    document.getElementById("mp_twi").href = "https://twitter.com/" + mp.twitter;
    document.getElementById("mp_twi").innerHTML = "@"+ mp.twitter;

	if (mp.twitter == undefined)
		document.getElementById("mp_twi").style.display = "none";
	else
		document.getElementById("mp_twi").style.display = "inline";
}

